# DUL Bitnami Drupal Docker Development Environment

## Bitnami Image Source

[https://github.com/bitnami/bitnami-docker-drupal](https://github.com/bitnami/bitnami-docker-drupal)

## Using This Image

### Start Drupal and MariaDB

```
$ docker-compose up
```

### Shell into image as root

```
$ docker exec -u 0 -it dul-bitnami-drupal_drupal_1 /bin/bash

```

### Default Drupal User/Password
```
User: user
Password: bitnami
```
